This is a repository to track my fulfillment of Flipstarter campaigns.

## Open

- [Freelancers Flipstarter transperncy report](https://gitlab.com/uak/flipstarter-transparency/-/issues/3)
- [For the Win 2 Flipstarter transperncy report](https://gitlab.com/uak/flipstarter-transparency/-/issues/2)

## Closed


- [For the Win 2 Flipstarter transperncy report](https://gitlab.com/uak/flipstarter-transparency/-/issues/1)